<?php

namespace stakantin\phpexcel;
class Initiator extends \AbstractController {
    public $phpexcel_public_location;

    function init() {
        parent::init();
        if(isset($this->app->phpexcel) && is_object($this->app->phpexcel)) {
            // do nothing
        } else {
            $this->app->phpexcel = $this;
        }

        // add add-on locations to pathfinder
        $addon_location = $this->app->locate('addons', __NAMESPACE__);
        $this->app->phpexcel_public_location=$this->owner->app->addLocation(array(
            'php' =>['lib','PHPExcel','lib/PHPExcel'],
        ))
            ->setBasePath($addon_location)
            ->setBaseURL('../'.$addon_location);
    }
}