<?php
/**
 *Created by Konstantin Kolodnitsky
 * Date: 28.07.14
 * Time: 17:51
 */
namespace stakantin\phpexcel;
class Controller_Phpexcel extends \AbstractController{
    public $objPHPExcel;
    public $filePath;
    public $settings = [
        'title'=>'ATK4 addon for PHPExcel',
        'creator'=>'ATK4',
        'lastModifiedBy'=>'ATK4',
        'subject'=>'xls file from ATK4',
        'description'=>'This file was created by ATK4 v.3 addon for PHPExcel'
    ];
    function init(){
        parent::init();

        $this->add('stakantin\phpexcel\Initiator');
    }
    public function createSheet(){
        $this->objPHPExcel = new \PHPExcel();

        // Set properties
        $this->objPHPExcel->getProperties()->setCreator($this->settings['creator']);
        $this->objPHPExcel->getProperties()->setLastModifiedBy($this->settings['lastModifiedBy']);
        $this->objPHPExcel->getProperties()->setTitle($this->settings['title']);
        $this->objPHPExcel->getProperties()->setSubject($this->settings['subject']);
        $this->objPHPExcel->getProperties()->setDescription($this->settings['description']);
        return $this->objPHPExcel;
    }
    public function readSheet(){
        $objReader = new \PHPExcel_Reader_Excel5();
        $this->objPHPExcel = $objReader->load($this->filePath);
        return $this->objPHPExcel;
    }
    public function saveSheet(){
        $objWriter = new \PHPExcel_Writer_Excel5($this->objPHPExcel);
        $objWriter->save($this->filePath);
    }

    //TODO OR NOT TODO?

    /*
    function getCellValue($col,$row){
        $val = $this->objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getValue();
        return $val;
    }
    function setCellValue($col,$row,$val){
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $val);
    }
    */
    function downloadFile(){
        // redirect output to client browser
//        header('Content-Type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment;filename='.$this->filePath);
//        header('Cache-Control: max-age=0');
        /*$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');*/

        // redirect output to client browser
//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment;filename="myfile.xlsx"');
//        header('Cache-Control: max-age=0');
//        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//        $objWriter->save('php://output');
    }
}