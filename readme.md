PHPExcel addon for ATK4.3
===========================

#Installation

Use composer.json

    {
        "require":{
            "stakantin\/phpexcel":"dev-master"
        }
    }

or

Clone it from https://bitbucket.org/stakantin/phpexcel

#How to use

    $xls_controller = $this->add('stakantin\phpexcel\Controller_Phpexcel',['filePath'=>$path,'settings'=>[
        'title'=>'File title',
        'creator'=>'Creator',
        'lastModifiedBy'=>'Name',
        'subject'=>'xls file from ATK4',
        'description'=>'This file was created by ATK4 v.3 addon for PHPExcel'
    ]]);

If you are creating new file use:

    $new_sheet = $xls_controller->createSheet();
    
If you want to read the existing file use:

    $new_sheet = $xls_controller->readSheet()

Then just use PHPExcel commands to that object ($new_sheet).

At the and save the file by using:

    $xls_controller->saveSheet();